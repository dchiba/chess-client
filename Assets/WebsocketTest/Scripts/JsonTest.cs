﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

public class JsonTest : MonoBehaviour {

	[Serializable]
	public class Message {
		public string message;

		public override string ToString() {
			return message;
		}
	}

	[Serializable]
	public class Position {
		public int X;
		public int Y;

		public override string ToString() {
			return string.Format("{{X: {0}, Y: {1}}}", X, Y);
		}
	}

	[Serializable]
	public class Positions {
		public List<Position> positionList = new List<Position>();

		public override string ToString () {
			return positionList.Select(p => p.ToString()).Aggregate((str, s) => str + ", " + s);
		}
	}

	void Start () {
		/* 基本的な JSON デコードエンコードの確認 */
		var p = new Position();
		p.X = 1;
		p.Y = 2;
		Debug.Log(p);

		var json = JsonUtility.ToJson(p);
		Debug.Log(json); //=> {"X":1,"Y":2}

		var p2 = JsonUtility.FromJson<Position>(json);
		Debug.Log(p2);

		/* ネストされたクラスのデコードエンコードの確認 */
		var positions = new Positions();
		positions.positionList.Add(p);
		positions.positionList.Add(p2);
		Debug.Log(positions);

		var json2 = JsonUtility.ToJson(positions);
		Debug.Log(json2); //=> {"positionList":[{"X":1,"Y":2},{"X":1,"Y":2}]}

		var ps2 = JsonUtility.FromJson<Positions>(json2);
		Debug.Log(ps2);

		/* FromJson 時に不要な要素は無視されることの確認 */
		var str = "{\"message\":\"any message\",\"X\":100,\"Y\":200}";
		Debug.Log(JsonUtility.FromJson<Message>(str));
		Debug.Log(JsonUtility.FromJson<Position>(str));
	}
}
