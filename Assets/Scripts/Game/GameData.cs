﻿using UnityEngine;
using System.Collections;
using System;

public class GameData : SingletonMonoBehaviour<GameData>
{

	public bool SinglePlayer;

	public enum Difficulty
	{
		Easy,
		Normal,
		Hard,
	}

	public Difficulty currentDifficulty;

	public enum Mode
	{
		Default,
		Mini,
		Micro,
	}

	public Mode CurrentMode;

	public bool IsDefaultMode ()
	{
		return CurrentMode == Mode.Default;
	}

	public bool IsMiniMode ()
	{
		return CurrentMode == Mode.Mini;
	}

	public bool IsMicroMode ()
	{
		return CurrentMode == Mode.Micro;
	}

	public PlayerInfo MyPlayer { get; set; }

	public Color Player1Color;
	public Color Player2Color;

	private InitializeResponse initializeResponse;

	public InitializeResponse InitializeResponse { 
		get { 
			return initializeResponse; 
		}
		set {
			initializeResponse = value;
			SentePlayer = initializeResponse.players [0];
			CurrentMode = (Mode)initializeResponse.mode;
		}
	}

	public void SetColors ()
	{
		var index1 = UnityEngine.Random.Range (0, Constants.KomaColors.Length);
		var param1 = Constants.KomaColors [index1];
		Player1Color = new Color (param1 [0] / 255f, param1 [1] / 255f, param1 [2] / 255f);

		var index2 = index1;
		while (index2 == index1)
		{
			index2 = UnityEngine.Random.Range (0, Constants.KomaColors.Length);
		}
		var param2 = Constants.KomaColors [index2];
		Player2Color = new Color (param2 [0] / 255f, param2 [1] / 255f, param2 [2] / 255f);
	}

	public PlayerInfo SentePlayer { get; private set; }

	private StartResponse startResponse;

	public StartResponse StartResponse {
		get {
			return startResponse;
		}
		set {
			startResponse = value;
			BaseServerUnixtime = startResponse.current_time;
			BaseClientDateTime = DateTime.Now;
		}
	}

	public long BaseServerUnixtime { get; private set; }

	public DateTime BaseClientDateTime { get; private set; }

	public double CurrentServerUnixtime ()
	{
		var diff = DateTime.Now - BaseClientDateTime;
		return BaseServerUnixtime + (diff.TotalMilliseconds / 1000);
	}

	public enum GamePhase
	{
		BeforeStart,
		Playing,
		SelectMovePosition,
		SelectPutPosition,
		Finished
	}

	public GamePhase CurrentGamePhase { get; set; }

	public bool IsPlaying ()
	{
		return CurrentGamePhase == GamePhase.Playing;
	}

	public bool IsSelectingMovePosition ()
	{
		return CurrentGamePhase == GamePhase.SelectMovePosition;
	}

	public bool IsSelectingPutPosition ()
	{
		return CurrentGamePhase == GamePhase.SelectPutPosition;
	}
}
