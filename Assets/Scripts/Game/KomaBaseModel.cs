﻿using UnityEngine;
using System.Collections;

public class KomaBaseModel : MonoBehaviour {

    [SerializeField] UISprite sprite;

    public KomaBaseInfo komaBaseInfo { get; private set; }

    public void SetKomaBaseInfo(KomaBaseInfo info) 
    {
        komaBaseInfo = info;
        var spriteNames = new string[]{
            "", "king", "queen", "rook", "bishop", "knight", "pawn"
        };
        var isSente = GameData.Instance.SentePlayer.id == info.player.id;
        var spriteName = "chess_" + spriteNames[komaBaseInfo.koma_type];
        sprite.spriteName = spriteName;

        if (!isSente)
        {
            transform.localRotation = Quaternion.Euler(0, 0, 180f);
            sprite.color = GameData.Instance.Player1Color;
        }
        else
        {
            sprite.color = GameData.Instance.Player2Color;
        }
    }
}
