﻿using UnityEngine;
using System.Collections;

public class FocusModel : MonoBehaviour {

    [SerializeField] UISprite sprite;

    public MovePositionInfo MovePositionInfo { get; set; }
    public string spriteName { get { return sprite.spriteName; } set { sprite.spriteName = value; } }

    public event System.Action<FocusModel, bool> onClickAction;
    public event System.Action<FocusModel> onClickSelectAction;

    public void OnClick()
    {
        if (MovePositionInfo.nari)
        {
            if (onClickSelectAction != null)
            {
                onClickSelectAction(this);
            }
        }
        else
        {
            if (onClickAction != null)
            {
                onClickAction(this, false);
            }
        }
    }
}
