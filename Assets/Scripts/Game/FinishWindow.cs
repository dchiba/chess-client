﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class FinishWindow : MonoBehaviour 
{
    [SerializeField] UISprite sprite;
    [SerializeField] UILabel descriptionLabel;

    public void SetFinishResponse(FinishResponse data)
    {
        var finishResponse = data;

        var gameData = GameData.Instance;

        if (gameData.MyPlayer.id == finishResponse.winner.id)
        {
            sprite.spriteName = "chess_WIN";
        }
        else
        {
            sprite.spriteName = "chess_LOSE";
        }
    }

    public void SetLeaveResponse(LeaveResponse data)
    {
        sprite.spriteName = "chess_WIN";
        descriptionLabel.text = "Your opponent user has left.";
    }

    public void OnClick()
    {
        NetworkManager.Instance.Close();

        SceneManager.LoadScene("Title");
    }
}
