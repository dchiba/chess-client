﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundManager : SingletonMonoBehaviour<SoundManager>
{

	bool enable;

	public bool Enable {
		get {
			return enable;
		}
		set {
			enable = value;
			audioSource.mute = !enable;
		}
	}

	private Dictionary<TYPE, string> audioResource;
	private AudioSource audioSource;
	private Dictionary<TYPE, AudioClip> audioClipCache;

	public enum TYPE
	{
		OK,
		NG,
		READY,
		START,
		MOVABLE,
		MOVE,
		MOVE_AND_CAPTURED,
		CLEAR,
		BGM_TITLE,
		BGM_MAIN,
	}

	public void Awake ()
	{
		if (this != Instance)
		{
			Destroy (this);
			return;
		}

		DontDestroyOnLoad (this.gameObject);

		audioResource = new Dictionary<TYPE, string> {
			// via http://www.kurage-kosho.info/others.html
			{ TYPE.OK, "Sounds/jump1" },
			{ TYPE.NG, "Sounds/jump1" },
			{ TYPE.MOVABLE, "Sounds/jump1" },
			{ TYPE.MOVE, "Sounds/jump2" },
			{ TYPE.MOVE_AND_CAPTURED, "Sounds/jump2" },
			{ TYPE.READY, "Sounds/laser2" },
			{ TYPE.START, "Sounds/laser1" },
			{ TYPE.CLEAR, "Sounds/laser1" },
			// via http://maoudamashii.jokersounds.com/list/bgm6.html
			{ TYPE.BGM_TITLE, "Sounds/bgm_maoudamashii_orchestra14" },
			{ TYPE.BGM_MAIN, "Sounds/bgm_maoudamashii_orchestra13" },
		};

		audioSource = gameObject.AddComponent<AudioSource> ();

		audioClipCache = new Dictionary<TYPE, AudioClip> ();

		// Enable = PlayerPrefs.GetInt (Const.PREF.SOUND_ENABLE.ToString (), 1) == 1 ? true : false;
	}

	public void ToggleActivate ()
	{
		Enable = !Enable;
		// PlayerPrefs.SetInt (Const.PREF.SOUND_ENABLE.ToString (), Enable ? 1 : 0);
	}

	public void PlaySE (TYPE t, float volume = 1f)
	{
//		if (!Enable)
//			return;

		AudioClip clip = GetAudioClip (t);

		audioSource.PlayOneShot (clip, volume);
	}

	public static void PlayOk ()
	{
		if (Instance != null)
		{
			Instance.PlaySE (TYPE.OK);
		}
	}

	public void PlayBGM (TYPE t)
	{
		audioSource.clip = GetAudioClip (t);
		audioSource.loop = true;
		audioSource.Play ();
	}

	AudioClip GetAudioClip (TYPE t)
	{
		AudioClip clip;
		if (!audioClipCache.ContainsKey (t))
		{
			clip = (AudioClip)Resources.Load (audioResource [t]);
			audioClipCache [t] = clip;
		}
		else
		{
			clip = audioClipCache [t];
		}
		return clip;
	}

}
