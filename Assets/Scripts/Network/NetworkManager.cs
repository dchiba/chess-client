﻿//#define ENABLE_OP_PLAYER

using UnityEngine;
using System.Collections;
using WebSocketSharp;
using System;

public class NetworkManager : SingletonMonoBehaviour<NetworkManager>
{

	[SerializeField] string url;
	[SerializeField] string developmentUrl;

	public WebSocket ws { get; private set; }

	public WebSocket ws2 { get; private set; }

	public event Action<JoinResponse> onReceiveJoinResponse;
	public event Action<InitializeResponse> onReceiveInitializeResponse;
	public event Action<StartResponse> onReceiveStartResponse;
	public event Action<MovablePositionResponse> onReceiveMovablePositionResponse;
	public event Action<MoveResponse> onReceiveMoveResponse;
	public event Action<FinishResponse> onReceiveFinishResponse;
	public event Action<ErrorResponse> onReceiveErrorResponse;
	public event Action<LeaveResponse> onReceiveLeaveResponse;

	public void Open ()
	{
		if (ws == null)
		{
			#if ENABLE_OP_PLAYER
            ws2 = Connect ();
			#endif

			ws = Connect ();            
		}
	}

	private WebSocket Connect ()
	{
		// Development Build のチェックの有無で接続先サーバーを変える
		// Editor の場合は常に開発サーバーに接続する
		var ws = new WebSocket (Debug.isDebugBuild ? developmentUrl : url);

		ws.OnOpen += (sender, e) => {
			Debug.Log ("Websocket Open");
		};
		ws.OnError += (sender, e) => {
			Debug.Log ("Websocket Error: " + e.Message);
			//TODO error handling
		};

		ws.OnMessage += OnMessage;
		ws.OnClose += (sender, e) => {
			Debug.Log ("Websocket Close ");
			//TODO error handling
		};

		Debug.Log ("Start Connect");
		ws.Connect ();
		Debug.Log ("End Connect");

		return ws;
	}

	private void OnMessage (object sender, MessageEventArgs e)
	{
		Debug.Log ("Websocket Message: " + e.Data);
		var list = e.Data.Split (new char[]{ ' ' }, 2);
		try
		{
            
			switch (list [0])
			{
			case Constants.JoinResponseMessage:
				if (onReceiveJoinResponse != null)
				{
					onReceiveJoinResponse (JsonUtility.FromJson<JoinResponse> (list [1]));
				}
				break;
			case Constants.InitializeResponseMessage:
				if (onReceiveInitializeResponse != null)
				{
					onReceiveInitializeResponse (JsonUtility.FromJson<InitializeResponse> (list [1]));
				}
				break;
			case Constants.StartResponseMessage:
				if (onReceiveStartResponse != null)
				{
					onReceiveStartResponse (JsonUtility.FromJson<StartResponse> (list [1]));
				}
				break;
			case Constants.MovablePositionsResponseMessage:
				if (onReceiveMovablePositionResponse != null)
				{
					onReceiveMovablePositionResponse (JsonUtility.FromJson<MovablePositionResponse> (list [1]));
				}
				break;
			case Constants.MoveResponseMessage:
				if (onReceiveMoveResponse != null)
				{
					onReceiveMoveResponse (JsonUtility.FromJson<MoveResponse> (list [1]));
				}
				break;
			case Constants.FinishResponseMessage:
				if (onReceiveFinishResponse != null)
				{
					onReceiveFinishResponse (JsonUtility.FromJson<FinishResponse> (list [1]));
				}
				break;
			case Constants.ErrorResponseMessage:
				if (onReceiveErrorResponse != null)
				{
					onReceiveErrorResponse (JsonUtility.FromJson<ErrorResponse> (list [1]));
				}
				break;
			case Constants.LeaveResponseMessage:
				if (onReceiveLeaveResponse != null)
				{
					onReceiveLeaveResponse (JsonUtility.FromJson<LeaveResponse> (list [1]));
				}
				break;
			default:
            //TODO error handling
				break;
			}

		}
		catch (Exception ex)
		{
			Debug.LogError (ex);
		}
	}

	public void Send<T> (string message, T data)
	{
		ws.Send (message + " " + JsonUtility.ToJson (data));

		#if ENABLE_OP_PLAYER
        if (message == Constants.JoinRequestMessage || message == Constants.InitializedRequestMessage) {
            ws2.Send(message + " " + JsonUtility.ToJson(data));
        }
		#endif
	}

	public void Close ()
	{
		if (ws != null)
		{
			ws.Close ();
			ws = null;
		}

		#if ENABLE_OP_PLAYER
        if (ws2 != null)
        {
            ws2.Close();
            ws2 = null;
        }
		#endif
	}

	void OnDestroy ()
	{
		Close ();
	}
}
