﻿using UnityEngine;
using System.Collections;
using System;

// RandomJoin

[Serializable]
public class RandomJoinRequest
{
	public int mode;
}

// CpuJoin
[Serializable]
public class CpuJoinRequest
{
	public int mode;
}

// Join

[Serializable]
public class JoinRequest
{
	public string name;
	public int mode;
}

[Serializable]
public class JoinResponse
{
	public PlayerInfo player;
}

// Initialize

[Serializable]
public class InitializeResponse
{
	public int mode;
	public int width;
	public int height;
	public PlayerInfo[] players;
	public StateInfo state;
}

// Start

[Serializable]
public class StartResponse
{
	public long current_time;
	public long start_time;
}

// MovablePositions

[Serializable]
public class MovablePositionsRequest
{
	public PositionInfo position;
}

[Serializable]
public class MovablePositionResponse
{
	public MovePositionInfo[] positions;
}

// Move

[Serializable]
public class MoveRequest
{
	public PositionInfo from;
	public PositionInfo to;
	public bool nari;
}

[Serializable]
public class MoveResponse
{
	public PlayerInfo player;
	public PositionInfo from;
	public PositionInfo to;
	public KomaInfo moved_koma;
	public bool captured;
	public KomaInfo removed_koma;
}

// CpuHand

[Serializable]
public class CpuHandRequest
{
	public int level;
}

// Finish

[Serializable]
public class FinishResponse
{
	public PlayerInfo winner;
}

// DebugSetTime

[Serializable]
public class DebugSetTimeRequest
{
	public long time;
}

// Empty

[Serializable]
public class EmptyRequest
{
    
}

// Error

[Serializable]
public class ErrorResponse
{
	public string msg;
}

// Leave

[Serializable]
public class LeaveResponse
{
	public PlayerInfo player;
}

// Info

[Serializable]
public class PlayerInfo
{
	public int id;
}

[Serializable]
public class KomaBaseInfo
{
	public int id;
	public int koma_type;
	public PlayerInfo player;
}

[Serializable]
public class KomaInfo
{
	public KomaBaseInfo koma_base;
	public long last_moved_time;
	public long movable_time;
	public PositionInfo position;
}

[Serializable]
public class PositionInfo
{
	public int x;
	public int y;

	public override bool Equals (System.Object obj)
	{
		if (obj == null) return false;
		PositionInfo p = obj as PositionInfo;
		if ((System.Object)p == null) return false;
		return (x == p.x) && (y == p.y);
	}

	public bool Equals (PositionInfo p)
	{
		if ((object)p == null) return false;
		return (x == p.x) && (y == p.y);
	}
}

[Serializable]
public class MovePositionInfo
{
	public PositionInfo position;
	public bool nari;
}

[Serializable]
public class StateInfo
{
	public KomaInfo[] ban;
}
