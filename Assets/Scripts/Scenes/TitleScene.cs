﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TitleScene : MonoBehaviour
{
	[SerializeField] GameObject playerSelectContainer;
	[SerializeField] GameObject modeSelectContainer;
	[SerializeField] GameObject roomSelectContainer;
	[SerializeField] GameObject difficultySelectContainer;

	[SerializeField] UIInput roomNameInput;

	[SerializeField] GameObject windowRoot;
	[SerializeField] GameObject waitJoinWindowPrefab;
	[SerializeField] GameObject errorWindowPrefab;

	private int mode;
	// 0: default, 1: mini, 2: micro

	private bool initialized = false;
	private ErrorResponse errorResponse = null;

	private GameObject window;

	void OnEnable ()
	{
		NetworkManager.Instance.onReceiveErrorResponse += OnReceiveErrorResponse;
		NetworkManager.Instance.onReceiveJoinResponse += OnReceiveJoinResponse;
		NetworkManager.Instance.onReceiveInitializeResponse += OnReceiveInitializeResponse;
	}

	void OnDisable ()
	{
		NetworkManager.Instance.onReceiveErrorResponse -= OnReceiveErrorResponse;
		NetworkManager.Instance.onReceiveJoinResponse -= OnReceiveJoinResponse;
		NetworkManager.Instance.onReceiveInitializeResponse -= OnReceiveInitializeResponse;
	}

	void Start ()
	{
		NetworkManager.Instance.Open ();
		SoundManager.Instance.PlayBGM (SoundManager.TYPE.BGM_TITLE);
		ChangePlayerSemectMenu ();
	}

	void Update ()
	{
		if (initialized)
		{
			SceneManager.LoadScene ("Main");
			initialized = false;
		}

		if (errorResponse != null)
		{
			if (errorResponse.msg == "too many player")
			{
				window = NGUITools.AddChild (windowRoot, errorWindowPrefab);
				window.GetComponentInChildren<UILabel> ().text = "too many players";
				window.GetComponentInChildren<UIButton> ().onClick.Add (new EventDelegate (this, "OnClickWindow"));
			}
			errorResponse = null;
		}
	}

	void ChangePlayerSemectMenu ()
	{
		ChangeSelectMenu (true, false, false, false);
	}

	void ChangeModeSemectMenu ()
	{
		ChangeSelectMenu (false, true, false, false);
	}

	void ChangeRoomSemectMenu ()
	{
		ChangeSelectMenu (false, false, true, false);
	}

	void ChangeDifficultySemectMenu ()
	{
		ChangeSelectMenu (false, false, false, true);
	}

	void ChangeSelectMenu (bool player, bool mode, bool room, bool difficulty)
	{
		NGUITools.SetActive (playerSelectContainer, player);
		NGUITools.SetActive (modeSelectContainer, mode);
		NGUITools.SetActive (roomSelectContainer, room);
		NGUITools.SetActive (difficultySelectContainer, difficulty);
	}

#region PlayerSelect

	public void OnClickSinglePlayer ()
	{
		GameData.Instance.SinglePlayer = true;
		ChangeModeSemectMenu ();
		SoundManager.PlayOk ();
	}

	public void OnClickDoublePlayer ()
	{
		GameData.Instance.SinglePlayer = false;
		ChangeModeSemectMenu ();
		SoundManager.PlayOk ();
	}

#endregion

#region ModeSelect

	public void OnClickDefaultMode ()
	{
		ChangeModeToNext (0);
	}

	public void OnClickMiniMode ()
	{
		ChangeModeToNext (1);
	}

	public void OnClickMicroMode ()
	{
		ChangeModeToNext (2);
	}

	public void OnClickBackToPlayerSelectMenu ()
	{
		ChangePlayerSemectMenu ();
		SoundManager.PlayOk ();
	}

	void ChangeModeToNext (int m)
	{
		mode = m;
		if (GameData.Instance.SinglePlayer)
		{
			ChangeDifficultySemectMenu ();
		}
		else
		{
			ChangeRoomSemectMenu ();
		}
		SoundManager.PlayOk ();
	}

#endregion

#region RoomSelect

	public void OnClickSendButton ()
	{
		window = NGUITools.AddChild (windowRoot, waitJoinWindowPrefab);
		window.GetComponentInChildren<UIButton> ().onClick.Add (new EventDelegate (this, "OnClickWindowAndConnection"));

		var name = roomNameInput.value;
		var joinRequest = new JoinRequest{ name = name, mode = mode };
		NetworkManager.Instance.Open ();
		NetworkManager.Instance.Send<JoinRequest> (Constants.JoinRequestMessage, joinRequest);
		SoundManager.Instance.PlaySE (SoundManager.TYPE.OK);
	}

	public void OnClickRandomButton ()
	{
		window = NGUITools.AddChild (windowRoot, waitJoinWindowPrefab);
		window.GetComponentInChildren<UIButton> ().onClick.Add (new EventDelegate (this, "OnClickWindowAndConnection"));

		NetworkManager.Instance.Open ();
		NetworkManager.Instance.Send<RandomJoinRequest> (Constants.RandomJoinRequestMessage, new RandomJoinRequest{ mode = mode });
		SoundManager.Instance.PlaySE (SoundManager.TYPE.OK);
	}

	public void OnClickWindow ()
	{
		Destroy (window);
		SoundManager.Instance.PlaySE (SoundManager.TYPE.OK);
	}

	public void OnClickWindowAndConnection ()
	{
		NetworkManager.Instance.Close ();
		NetworkManager.Instance.Open ();
		Destroy (window);
		SoundManager.Instance.PlaySE (SoundManager.TYPE.OK);
	}

	public void OnClickBackToModeSelectMenu ()
	{
		ChangeModeSemectMenu ();
		SoundManager.PlayOk ();
	}

#endregion

#region DifficultySelect

	public void OnClickEasy ()
	{
		GameData.Instance.currentDifficulty = GameData.Difficulty.Easy;
		SendCpuJoinRequest ();
	}

	public void OnClickNormal ()
	{
		GameData.Instance.currentDifficulty = GameData.Difficulty.Normal;
		SendCpuJoinRequest ();
	}

	public void OnClickHard ()
	{
		GameData.Instance.currentDifficulty = GameData.Difficulty.Hard;
		SendCpuJoinRequest ();
	}

	public void SendCpuJoinRequest ()
	{
		window = NGUITools.AddChild (windowRoot, waitJoinWindowPrefab);
		window.GetComponentInChildren<UIButton> ().onClick.Add (new EventDelegate (this, "OnClickWindowAndConnection"));

		NetworkManager.Instance.Open ();
		NetworkManager.Instance.Send<CpuJoinRequest> (Constants.CpuJoinRequestMessage, new CpuJoinRequest{ mode = mode });
		SoundManager.Instance.PlaySE (SoundManager.TYPE.OK);
	}

#endregion

	public void OnReceiveJoinResponse (JoinResponse data)
	{
		Debug.LogWarning ("(TitleScene) Receive JoinResponse");
		Debug.LogWarning ("(TitleScene) player id : " + data.player.id.ToString ());
		GameData.Instance.MyPlayer = data.player;
	}

	public void OnReceiveInitializeResponse (InitializeResponse data)
	{
		Debug.LogWarning ("(TitleScene) Receive InitializeResponse");
		GameData.Instance.InitializeResponse = data;
		initialized = true;
	}

	public void OnReceiveErrorResponse (ErrorResponse data)
	{
		Debug.LogWarning ("(TitleScene) Receive ErrorResponse");
		errorResponse = data;
	}
}
