﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MainScene : MonoBehaviour
{
	[SerializeField] GameObject mainCamera;
	[SerializeField] UIRoot uiroot;
	[SerializeField] GameObject banRootObject;
	[SerializeField] GameObject banFramePrefab;
	[SerializeField] GameObject banPrefab;
	[SerializeField] GameObject komaPrefab;
	[SerializeField] GameObject focusPrefab;

	[SerializeField] GameObject windowRoot;
	[SerializeField] GameObject readyWindowPrefab;
	[SerializeField] GameObject startWindowPrefab;
	[SerializeField] GameObject selectNariWindowPrefab;
	[SerializeField] GameObject finishWindowPrefab;

	private bool singlePlayerMode;
	private GameData.Difficulty currentDifficulty;
	private float cpuHandInterval = 1f;
	private float cpuHandDeltaTime = 0;

	private int banWidth;
	private int banHeight;

	private GameObject readyWindow;

	private GameData gameData;

	private List<KomaModel> komaModelList;

	private PositionInfo selectingPositionInfo;
	private List<FocusModel> focusModelList;
	private MovablePositionResponse preparedMovablePositionResponse;

	private List<MoveResponse> preparedMoveResponseList;

	private FinishResponse preparedFinishResponse;

	private LeaveResponse preparedLeaveResponse;

	void OnEnable ()
	{
		EnableReceiver ();
	}

	void OnDisable ()
	{
		DisableReceiver ();
	}

	void EnableReceiver ()
	{
		var networkManager = NetworkManager.Instance;
		if (networkManager != null)
		{
			networkManager.onReceiveStartResponse += OnReceiveStartResponse;
			networkManager.onReceiveMovablePositionResponse += OnReceiveMovablePositionsResponse;
			networkManager.onReceiveMoveResponse += OnReceiveMoveResponse;
			networkManager.onReceiveFinishResponse += OnReceiveFinish;
			networkManager.onReceiveLeaveResponse += OnReceiveLeave;
		}
	}

	void DisableReceiver ()
	{
		var networkManager = NetworkManager.Instance;
		if (networkManager != null)
		{
			networkManager.onReceiveStartResponse -= OnReceiveStartResponse;
			networkManager.onReceiveMovablePositionResponse -= OnReceiveMovablePositionsResponse;
			networkManager.onReceiveMoveResponse -= OnReceiveMoveResponse;
			networkManager.onReceiveFinishResponse -= OnReceiveFinish;
			networkManager.onReceiveLeaveResponse -= OnReceiveLeave;
		}
	}

	IEnumerator Start ()
	{
		SoundManager.Instance.PlayBGM (SoundManager.TYPE.BGM_MAIN);

		readyWindow = NGUITools.AddChild (windowRoot, readyWindowPrefab);
		SoundManager.Instance.PlaySE (SoundManager.TYPE.READY, 5f);

		gameData = GameData.Instance;
		gameData.CurrentGamePhase = GameData.GamePhase.BeforeStart;

		if (gameData.InitializeResponse == null)
		{
			CreateDebugData ();
		}

//        if (gameData.IsMicroMode()) {
//            NGUITools.SetActive(readyWindow.transform.GetChild(0).gameObject, true);
//        }

		if (gameData.SentePlayer.id != gameData.MyPlayer.id)
		{
			mainCamera.transform.localRotation = Quaternion.Euler (0, 0, 180);
		}

		komaModelList = new List<KomaModel> ();
		focusModelList = new List<FocusModel> ();
		preparedMoveResponseList = new List<MoveResponse> ();

		// InitializeResponse から盤と駒の描画
		gameData.SetColors ();
		SetupBan (gameData.InitializeResponse);
		UpdateBan (gameData.InitializeResponse.state);

		// InitializedRequest の送信
		NetworkManager.Instance.Send<EmptyRequest> (Constants.InitializedRequestMessage, new EmptyRequest ());
		yield return new WaitUntil (() => gameData.StartResponse != null);

		singlePlayerMode = gameData.SinglePlayer;
		currentDifficulty = gameData.currentDifficulty;
		cpuHandInterval = currentDifficulty == GameData.Difficulty.Easy ? 5f : currentDifficulty == GameData.Difficulty.Normal ? 3f : 1f;
		// モードごとに補正
		var rate = gameData.CurrentMode == GameData.Mode.Default ? 1f : gameData.CurrentMode == GameData.Mode.Mini ? 1.5f : 2f;
		cpuHandInterval *= rate;

		StartCoroutine (StartGame ());
	}

	void Update ()
	{
		if (preparedMovablePositionResponse != null)
		{
			CreateFocuses ();
		}

		if (preparedMoveResponseList.Count > 0 && preparedMoveResponseList [0] != null)
		{
			var data = preparedMoveResponseList [0];
			preparedMoveResponseList.Remove (data);
			Debug.Log (data);
			Move (data);
		}

		if (preparedFinishResponse != null)
		{
			Finish ();
		}

		if (preparedLeaveResponse != null)
		{
			Leave ();
		}

		if (singlePlayerMode)
		{
			CpuHand ();
		}
	}


#region Initialize

	IEnumerator StartGame ()
	{
		yield return new WaitUntil (() => gameData.StartResponse.start_time < gameData.CurrentServerUnixtime ());

		Destroy (readyWindow);

		var startWindow = NGUITools.AddChild (windowRoot, startWindowPrefab);
		Destroy (startWindow, 1f);

		SoundManager.Instance.PlaySE (SoundManager.TYPE.START, 5f);

		gameData.CurrentGamePhase = GameData.GamePhase.Playing;
	}

	void SetupBan (InitializeResponse data)
	{
		banWidth = data.width;
		banHeight = data.height;
		for (int y = 0; y < banHeight; y++)
		{
			for (int x = 0; x < banWidth; x++)
			{
				CreateFrame (new PositionInfo{ x = x, y = y });
			}
		}

		var index = (int)gameData.CurrentMode;

		uiroot.manualWidth = Constants.UIRootWidth [index];

		var cameraPositionVal = Constants.CameraPositions [index];
		mainCamera.transform.localPosition = new Vector3 (cameraPositionVal [0], cameraPositionVal [1], cameraPositionVal [2]);
	}

	void UpdateBan (StateInfo stateInfo)
	{
		foreach (var komaModel in komaModelList)
		{
			GameObject.Destroy (komaModel.gameObject);
		}
		komaModelList = new List<KomaModel> ();
		foreach (var komaInfo in stateInfo.ban)
		{
			CreateKoma (komaInfo);
		}
	}

	Vector3 BanPositonToVector3 (PositionInfo positionInfo)
	{
		return PositionInfoToVector3 (positionInfo, 88f, 96f, (banWidth - 1) / 2, (banHeight - 1) / 2);
	}

	Vector3 PositionInfoToVector3 (PositionInfo positionInfo, float width, float height, int offsetX, int offsetY)
	{
		return PositionInfoToVector3 (positionInfo.x, positionInfo.y, width, height, offsetX, offsetY);
	}

	Vector3 PositionInfoToVector3 (int x, int y, float width, float height, int offsetX, int offsetY)
	{
		return new Vector3 (
			(x * -1 + offsetX) * width,
			(y * -1 + offsetY) * height
		);
	}

	private void CreateFrame (PositionInfo info)
	{
		var go = NGUITools.AddChild (banRootObject, banFramePrefab);
		go.transform.localPosition = BanPositonToVector3 (info);
	}

	private void CreateKoma (KomaInfo komaInfo)
	{
		var position = BanPositonToVector3 (komaInfo.position);
		var komaObj = NGUITools.AddChild (banRootObject, komaPrefab);
		komaObj.transform.localPosition = position;
		var komaModel = komaObj.GetComponent<KomaModel> ();
		komaModel.SetKomaInfo (komaInfo);
		komaModel.enabled = true;
		komaModel.onClickAction += OnClickKomaModel;
		komaModelList.Add (komaModel);
	}

#endregion


#region OnClick

	public void OnClickKomaModel (KomaModel komaModel)
	{
		Debug.LogWarning (string.Format ("koma {0} {1}", komaModel.KomaInfo.koma_base.id, komaModel.Movable));
		// movable_positions の送信
		if (gameData.IsPlaying () || gameData.IsSelectingMovePosition () || gameData.IsSelectingPutPosition ())
		{
			selectingPositionInfo = komaModel.KomaInfo.position;
			NetworkManager.Instance.Send<MovablePositionsRequest> (
				Constants.MovablePositionsRequestMessage,
				new MovablePositionsRequest{ position = komaModel.KomaInfo.position }
			);
		}

		SoundManager.Instance.PlaySE (SoundManager.TYPE.OK);
	}

	public void OnClickFocusModel (FocusModel focusModel, bool nari)
	{
		if (!gameData.IsSelectingMovePosition ()) return;

		NetworkManager.Instance.Send<MoveRequest> (
			Constants.MoveRequestMessage,
			new MoveRequest {
				from = selectingPositionInfo,
				to = focusModel.MovePositionInfo.position,
				nari = nari
			}
		);

		SoundManager.Instance.PlaySE (SoundManager.TYPE.OK);
	}

	public void OnClickSelectFocusModel (FocusModel focusModel)
	{
		if (!gameData.IsSelectingMovePosition ()) return;

		var obj = NGUITools.AddChild (windowRoot, selectNariWindowPrefab);
		var wnd = obj.GetComponent<SelectNariWindow> ();
		wnd.focusModel = focusModel;
		wnd.onClickAction += OnClickFocusModel;

		SoundManager.Instance.PlaySE (SoundManager.TYPE.OK);
	}

#endregion


#region Response Handlers

	public void OnReceiveStartResponse (StartResponse data)
	{
		gameData.StartResponse = data;
	}

	public void OnReceiveMovablePositionsResponse (MovablePositionResponse data)
	{
		if (gameData.IsPlaying () || gameData.IsSelectingMovePosition () || gameData.IsSelectingPutPosition ())
		{
			gameData.CurrentGamePhase = GameData.GamePhase.SelectMovePosition;

			CreateFocusPrepare (data);
		}
	}

	public void OnReceiveMoveResponse (MoveResponse data)
	{
		Debug.Log (data);
		MovePrepare (data);
	}

	public void OnReceiveFinish (FinishResponse data)
	{
		FinishPrepare (data);
	}

	public void OnReceiveLeave (LeaveResponse data)
	{
		LeavePrepare (data);
	}

#endregion


#region MovablePositions

	// Websocket.OnMessage のコールバック内では Instantiate できないので準備だけする
	private void CreateFocusPrepare (MovablePositionResponse data)
	{
		preparedMovablePositionResponse = data;
	}

	private void CreateFocuses ()
	{
		var data = preparedMovablePositionResponse;
		preparedMovablePositionResponse = null;

		ClearForcuses ();

		foreach (var movePositionInfo in data.positions)
		{
			CreateFocus (movePositionInfo, Constants.FocusSpriteName, OnClickFocusModel, OnClickSelectFocusModel);
		}
		CreateFocus (new MovePositionInfo{ position = selectingPositionInfo, nari = false }, Constants.SelectedFocusSpriteName);
	}

	private void CreateFocus (MovePositionInfo movePositionInfo, string spriteName, Action<FocusModel, bool> onClick = null, Action<FocusModel> onClickSelect = null)
	{
		var pos = BanPositonToVector3 (movePositionInfo.position);
		var focusObj = NGUITools.AddChild (banRootObject, focusPrefab);
		focusObj.transform.localPosition = pos;
		var focusModel = focusObj.GetComponent<FocusModel> ();
		focusModel.MovePositionInfo = movePositionInfo;
		focusModel.spriteName = spriteName;
		if (onClick != null) focusModel.onClickAction += onClick;
		if (onClickSelect != null) focusModel.onClickSelectAction += onClickSelect;
		focusModelList.Add (focusModel);
	}

	private void ClearForcuses ()
	{
		foreach (var focusModel in focusModelList)
		{
			Destroy (focusModel.gameObject);
		}
		focusModelList.Clear ();
	}

#endregion


#region Move

	private void MovePrepare (MoveResponse data)
	{
		preparedMoveResponseList.Add (data);
	}

	private void Move (MoveResponse data)
	{
		if (data == null) return;

//        if (data.player.id == gameData.MyPlayer.id && selectingPositionInfo == null)
//        {
//            return;
//        }

		// 選択した駒が動いたら移動フォーカスをクリアする
		if (data.from.Equals (selectingPositionInfo))
		{
			selectingPositionInfo = null;
			ClearForcuses ();
			gameData.CurrentGamePhase = GameData.GamePhase.Playing;
		}

		// 選択した駒が取られたら移動フォーカスをクリア
		if (data.to.Equals (selectingPositionInfo))
		{
			ClearForcuses ();
		}

		KomaModel movedKomaModel = null;
		KomaModel removedKomaModel = null;
		for (int i = 0; i < komaModelList.Count; i++)
		{
			var komaModel = komaModelList [i];
			if (komaModel.KomaInfo.koma_base.id == data.moved_koma.koma_base.id)
			{
				movedKomaModel = komaModel;
			}
			if (data.captured && komaModel.KomaInfo.koma_base.id == data.removed_koma.koma_base.id)
			{
				removedKomaModel = komaModel;
			}
		}

		if (data.captured)
		{
			komaModelList.Remove (removedKomaModel);
			GameObject.Destroy (removedKomaModel.gameObject);
		}

		movedKomaModel.Move (
			BanPositonToVector3 (data.from),
			BanPositonToVector3 (data.to)
		);

		movedKomaModel.SetKomaInfo (data.moved_koma);

		if (data.captured)
		{
			SoundManager.Instance.PlaySE (SoundManager.TYPE.MOVE_AND_CAPTURED, 3f);
		}
		else
		{
			SoundManager.Instance.PlaySE (SoundManager.TYPE.MOVE);
		}
	}

#endregion


#region Finish

	private void FinishPrepare (FinishResponse data)
	{
		preparedFinishResponse = data;
	}

	private void Finish ()
	{
		gameData.CurrentGamePhase = GameData.GamePhase.Finished;

		var obj = NGUITools.AddChild (windowRoot, finishWindowPrefab);
		var wnd = obj.GetComponent<FinishWindow> ();
		wnd.SetFinishResponse (preparedFinishResponse);

		SoundManager.Instance.PlaySE (SoundManager.TYPE.CLEAR, 5f);

		preparedFinishResponse = null;

		DisableReceiver ();
	}

#endregion


#region Leave

	private void LeavePrepare (LeaveResponse data)
	{
		preparedLeaveResponse = data;
	}

	private void Leave ()
	{
		gameData.CurrentGamePhase = GameData.GamePhase.Finished;

		var obj = NGUITools.AddChild (windowRoot, finishWindowPrefab);
		var wnd = obj.GetComponent<FinishWindow> ();
		wnd.SetLeaveResponse (preparedLeaveResponse);

		SoundManager.Instance.PlaySE (SoundManager.TYPE.CLEAR, 5f);

		preparedLeaveResponse = null;
	}

#endregion

#region CpuHand

	private void CpuHand ()
	{
		cpuHandDeltaTime += Time.deltaTime;

		if (cpuHandDeltaTime >= cpuHandInterval)
		{
			cpuHandDeltaTime = 0;
			NetworkManager.Instance.Send<CpuHandRequest> (Constants.CpuHandRequestMessage, new CpuHandRequest{ level = (int)currentDifficulty });

			Debug.Log ("Send CpuHand Request");
		}
	}

#endregion

	void CreateDebugData ()
	{
		// Default
		// var json = "{\"width\":9,\"height\":9,\"mode\":0,\"players\":[{\"id\":5},{\"id\":6}],\"state\":{\"ban\":[{\"koma_base\":{\"id\":1,\"koma_type\":7,\"player\":{\"id\":6}},\"movable_time\":1467604111,\"position\":{\"x\":0,\"y\":0}},{\"koma_base\":{\"id\":2,\"koma_type\":6,\"player\":{\"id\":6}},\"movable_time\":1467604111,\"position\":{\"x\":1,\"y\":0}},{\"koma_base\":{\"id\":3,\"koma_type\":5,\"player\":{\"id\":6}},\"movable_time\":1467604111,\"position\":{\"x\":2,\"y\":0}},{\"koma_base\":{\"id\":4,\"koma_type\":4,\"player\":{\"id\":6}},\"movable_time\":1467604111,\"position\":{\"x\":3,\"y\":0}},{\"koma_base\":{\"id\":5,\"koma_type\":1,\"player\":{\"id\":6}},\"movable_time\":1467604111,\"position\":{\"x\":4,\"y\":0}},{\"koma_base\":{\"id\":6,\"koma_type\":4,\"player\":{\"id\":6}},\"movable_time\":1467604111,\"position\":{\"x\":5,\"y\":0}},{\"koma_base\":{\"id\":7,\"koma_type\":5,\"player\":{\"id\":6}},\"movable_time\":1467604111,\"position\":{\"x\":6,\"y\":0}},{\"koma_base\":{\"id\":8,\"koma_type\":6,\"player\":{\"id\":6}},\"movable_time\":1467604111,\"position\":{\"x\":7,\"y\":0}},{\"koma_base\":{\"id\":9,\"koma_type\":7,\"player\":{\"id\":6}},\"movable_time\":1467604111,\"position\":{\"x\":8,\"y\":0}},{\"koma_base\":{\"id\":10,\"koma_type\":3,\"player\":{\"id\":6}},\"movable_time\":1467604111,\"position\":{\"x\":1,\"y\":1}},{\"koma_base\":{\"id\":11,\"koma_type\":2,\"player\":{\"id\":6}},\"movable_time\":1467604111,\"position\":{\"x\":7,\"y\":1}},{\"koma_base\":{\"id\":12,\"koma_type\":8,\"player\":{\"id\":6}},\"movable_time\":1467604111,\"position\":{\"x\":0,\"y\":2}},{\"koma_base\":{\"id\":13,\"koma_type\":8,\"player\":{\"id\":6}},\"movable_time\":1467604111,\"position\":{\"x\":1,\"y\":2}},{\"koma_base\":{\"id\":14,\"koma_type\":8,\"player\":{\"id\":6}},\"movable_time\":1467604111,\"position\":{\"x\":2,\"y\":2}},{\"koma_base\":{\"id\":15,\"koma_type\":8,\"player\":{\"id\":6}},\"movable_time\":1467604111,\"position\":{\"x\":3,\"y\":2}},{\"koma_base\":{\"id\":16,\"koma_type\":8,\"player\":{\"id\":6}},\"movable_time\":1467604111,\"position\":{\"x\":4,\"y\":2}},{\"koma_base\":{\"id\":17,\"koma_type\":8,\"player\":{\"id\":6}},\"movable_time\":1467604111,\"position\":{\"x\":5,\"y\":2}},{\"koma_base\":{\"id\":18,\"koma_type\":8,\"player\":{\"id\":6}},\"movable_time\":1467604111,\"position\":{\"x\":6,\"y\":2}},{\"koma_base\":{\"id\":19,\"koma_type\":8,\"player\":{\"id\":6}},\"movable_time\":1467604111,\"position\":{\"x\":7,\"y\":2}},{\"koma_base\":{\"id\":20,\"koma_type\":8,\"player\":{\"id\":6}},\"movable_time\":1467604111,\"position\":{\"x\":8,\"y\":2}},{\"koma_base\":{\"id\":21,\"koma_type\":8,\"player\":{\"id\":5}},\"movable_time\":1467604111,\"position\":{\"x\":0,\"y\":6}},{\"koma_base\":{\"id\":22,\"koma_type\":8,\"player\":{\"id\":5}},\"movable_time\":1467604111,\"position\":{\"x\":1,\"y\":6}},{\"koma_base\":{\"id\":23,\"koma_type\":8,\"player\":{\"id\":5}},\"movable_time\":1467604111,\"position\":{\"x\":2,\"y\":6}},{\"koma_base\":{\"id\":24,\"koma_type\":8,\"player\":{\"id\":5}},\"movable_time\":1467604111,\"position\":{\"x\":3,\"y\":6}},{\"koma_base\":{\"id\":25,\"koma_type\":8,\"player\":{\"id\":5}},\"movable_time\":1467604111,\"position\":{\"x\":4,\"y\":6}},{\"koma_base\":{\"id\":26,\"koma_type\":8,\"player\":{\"id\":5}},\"movable_time\":1467604111,\"position\":{\"x\":5,\"y\":6}},{\"koma_base\":{\"id\":27,\"koma_type\":8,\"player\":{\"id\":5}},\"movable_time\":1467604111,\"position\":{\"x\":6,\"y\":6}},{\"koma_base\":{\"id\":28,\"koma_type\":8,\"player\":{\"id\":5}},\"movable_time\":1467604111,\"position\":{\"x\":7,\"y\":6}},{\"koma_base\":{\"id\":29,\"koma_type\":8,\"player\":{\"id\":5}},\"movable_time\":1467604111,\"position\":{\"x\":8,\"y\":6}},{\"koma_base\":{\"id\":30,\"koma_type\":2,\"player\":{\"id\":5}},\"movable_time\":1467604111,\"position\":{\"x\":1,\"y\":7}},{\"koma_base\":{\"id\":31,\"koma_type\":3,\"player\":{\"id\":5}},\"movable_time\":1467604111,\"position\":{\"x\":7,\"y\":7}},{\"koma_base\":{\"id\":32,\"koma_type\":7,\"player\":{\"id\":5}},\"movable_time\":1467604111,\"position\":{\"x\":0,\"y\":8}},{\"koma_base\":{\"id\":33,\"koma_type\":6,\"player\":{\"id\":5}},\"movable_time\":1467604111,\"position\":{\"x\":1,\"y\":8}},{\"koma_base\":{\"id\":34,\"koma_type\":5,\"player\":{\"id\":5}},\"movable_time\":1467604111,\"position\":{\"x\":2,\"y\":8}},{\"koma_base\":{\"id\":35,\"koma_type\":4,\"player\":{\"id\":5}},\"movable_time\":1467604111,\"position\":{\"x\":3,\"y\":8}},{\"koma_base\":{\"id\":36,\"koma_type\":1,\"player\":{\"id\":5}},\"movable_time\":1467604111,\"position\":{\"x\":4,\"y\":8}},{\"koma_base\":{\"id\":37,\"koma_type\":4,\"player\":{\"id\":5}},\"movable_time\":1467604111,\"position\":{\"x\":5,\"y\":8}},{\"koma_base\":{\"id\":38,\"koma_type\":5,\"player\":{\"id\":5}},\"movable_time\":1467604111,\"position\":{\"x\":6,\"y\":8}},{\"koma_base\":{\"id\":39,\"koma_type\":6,\"player\":{\"id\":5}},\"movable_time\":1467604111,\"position\":{\"x\":7,\"y\":8}},{\"koma_base\":{\"id\":40,\"koma_type\":7,\"player\":{\"id\":5}},\"movable_time\":1467604111,\"position\":{\"x\":8,\"y\":8}}]}}";

		// Mini
		//var json = "{\"mode\":1,\"width\":5,\"height\":6,\"players\":[{\"id\":1},{\"id\":2}],\"state\":{\"ban\":[{\"koma_base\":{\"id\":1,\"koma_type\":5,\"player\":{\"id\":2}},\"last_moved_time\":1470967217,\"movable_time\":1470967222,\"position\":{\"x\":0,\"y\":0}},{\"koma_base\":{\"id\":2,\"koma_type\":4,\"player\":{\"id\":2}},\"last_moved_time\":1470967217,\"movable_time\":1470967222,\"position\":{\"x\":1,\"y\":0}},{\"koma_base\":{\"id\":3,\"koma_type\":1,\"player\":{\"id\":2}},\"last_moved_time\":1470967217,\"movable_time\":1470967224,\"position\":{\"x\":2,\"y\":0}},{\"koma_base\":{\"id\":4,\"koma_type\":4,\"player\":{\"id\":2}},\"last_moved_time\":1470967217,\"movable_time\":1470967222,\"position\":{\"x\":3,\"y\":0}},{\"koma_base\":{\"id\":5,\"koma_type\":5,\"player\":{\"id\":2}},\"last_moved_time\":1470967217,\"movable_time\":1470967222,\"position\":{\"x\":4,\"y\":0}},{\"koma_base\":{\"id\":6,\"koma_type\":8,\"player\":{\"id\":2}},\"last_moved_time\":1470967217,\"movable_time\":1470967220,\"position\":{\"x\":1,\"y\":2}},{\"koma_base\":{\"id\":7,\"koma_type\":8,\"player\":{\"id\":2}},\"last_moved_time\":1470967217,\"movable_time\":1470967220,\"position\":{\"x\":2,\"y\":2}},{\"koma_base\":{\"id\":8,\"koma_type\":8,\"player\":{\"id\":2}},\"last_moved_time\":1470967217,\"movable_time\":1470967220,\"position\":{\"x\":3,\"y\":2}},{\"koma_base\":{\"id\":9,\"koma_type\":8,\"player\":{\"id\":1}},\"last_moved_time\":1470967217,\"movable_time\":1470967220,\"position\":{\"x\":1,\"y\":3}},{\"koma_base\":{\"id\":10,\"koma_type\":8,\"player\":{\"id\":1}},\"last_moved_time\":1470967217,\"movable_time\":1470967220,\"position\":{\"x\":2,\"y\":3}},{\"koma_base\":{\"id\":11,\"koma_type\":8,\"player\":{\"id\":1}},\"last_moved_time\":1470967217,\"movable_time\":1470967220,\"position\":{\"x\":3,\"y\":3}},{\"koma_base\":{\"id\":12,\"koma_type\":5,\"player\":{\"id\":1}},\"last_moved_time\":1470967217,\"movable_time\":1470967222,\"position\":{\"x\":0,\"y\":5}},{\"koma_base\":{\"id\":13,\"koma_type\":4,\"player\":{\"id\":1}},\"last_moved_time\":1470967217,\"movable_time\":1470967222,\"position\":{\"x\":1,\"y\":5}},{\"koma_base\":{\"id\":14,\"koma_type\":1,\"player\":{\"id\":1}},\"last_moved_time\":1470967217,\"movable_time\":1470967224,\"position\":{\"x\":2,\"y\":5}},{\"koma_base\":{\"id\":15,\"koma_type\":4,\"player\":{\"id\":1}},\"last_moved_time\":1470967217,\"movable_time\":1470967222,\"position\":{\"x\":3,\"y\":5}},{\"koma_base\":{\"id\":16,\"koma_type\":5,\"player\":{\"id\":1}},\"last_moved_time\":1470967217,\"movable_time\":1470967222,\"position\":{\"x\":4,\"y\":5}}]}}";

		// Micro
		var json = "{\"mode\":2,\"width\":3,\"height\":4,\"players\":[{\"id\":7},{\"id\":8}],\"state\":{\"ban\":[{\"koma_base\":{\"id\":1,\"koma_type\":16,\"player\":{\"id\":8}},\"last_moved_time\":1470972164,\"movable_time\":1470972169,\"position\":{\"x\":0,\"y\":0}},{\"koma_base\":{\"id\":2,\"koma_type\":1,\"player\":{\"id\":8}},\"last_moved_time\":1470972164,\"movable_time\":1470972171,\"position\":{\"x\":1,\"y\":0}},{\"koma_base\":{\"id\":3,\"koma_type\":15,\"player\":{\"id\":8}},\"last_moved_time\":1470972164,\"movable_time\":1470972169,\"position\":{\"x\":2,\"y\":0}},{\"koma_base\":{\"id\":4,\"koma_type\":8,\"player\":{\"id\":8}},\"last_moved_time\":1470972164,\"movable_time\":1470972167,\"position\":{\"x\":1,\"y\":1}},{\"koma_base\":{\"id\":5,\"koma_type\":8,\"player\":{\"id\":7}},\"last_moved_time\":1470972164,\"movable_time\":1470972167,\"position\":{\"x\":1,\"y\":2}},{\"koma_base\":{\"id\":6,\"koma_type\":15,\"player\":{\"id\":7}},\"last_moved_time\":1470972164,\"movable_time\":1470972169,\"position\":{\"x\":0,\"y\":3}},{\"koma_base\":{\"id\":7,\"koma_type\":1,\"player\":{\"id\":7}},\"last_moved_time\":1470972164,\"movable_time\":1470972171,\"position\":{\"x\":1,\"y\":3}},{\"koma_base\":{\"id\":8,\"koma_type\":16,\"player\":{\"id\":7}},\"last_moved_time\":1470972164,\"movable_time\":1470972169,\"position\":{\"x\":2,\"y\":3}}]}}";

		var data = JsonUtility.FromJson<InitializeResponse> (json);
		gameData.InitializeResponse = data;
		gameData.MyPlayer = data.players [0];
	}
}
