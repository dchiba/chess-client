﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;
using WebSocketSharp;
using System.Linq;
using System;

public class WSTest : MonoBehaviour {

    string url = "ws://localhost:8080/ws";
    WebSocket ws1;
    WebSocket ws2;
    string room_id;
    EventHandler<MessageEventArgs> eventHandler;
    EventHandler<MessageEventArgs> nextEventHandler;

    private EventHandler<MessageEventArgs> CreateEventHandler<T> (Action<T> action) {
        EventHandler<MessageEventArgs> h = (object sender, MessageEventArgs e) => {
            try {
                var list = e.Data.Split(new char[]{' '}, 2);
                var data = JsonUtility.FromJson<T>(list[1]);
                action(data);
            } catch (Exception ex) {
                Debug.Log(ex.Message);
            }
        };
        return h;
    }


    IEnumerator Start() {
        Initialize ();

        // プレイヤー 1 が JoinRequest
        // JoinResponse を受け取る
        PlayerInfo player1;
        {
            JoinResponse res = null;
            eventHandler = CreateEventHandler<JoinResponse>((JoinResponse obj) => {
                res = obj;
                ws1.OnMessage -= eventHandler;
            });
            ws1.OnMessage += eventHandler;

            ws1.Send("join " + JsonUtility.ToJson(new JoinRequest{name = room_id}));
            yield return new WaitUntil( () => res != null );
            Debug.Log("Receive JoinResponse");

            Assert.IsNotNull<JoinResponse>(res);
            Assert.IsTrue(res.player.id > 0);
            player1 = res.player;
        }

        // プレイヤー 2 が JoinRequest
        // JoinResponse と InitializeResonse を受け取る
        PlayerInfo player2;
        InitializeResponse initializeResponse = null;
        {
            JoinResponse res = null;
            eventHandler = CreateEventHandler<JoinResponse>((JoinResponse obj) => {
                res = obj;
                ws2.OnMessage -= eventHandler;
                ws2.OnMessage += nextEventHandler;
            });
            nextEventHandler = CreateEventHandler<InitializeResponse>((InitializeResponse obj) => {
                initializeResponse = obj;
                ws2.OnMessage -= nextEventHandler;
            });
            ws2.OnMessage += eventHandler;

            ws2.Send("join " + JsonUtility.ToJson(new JoinRequest{name = room_id}));
            yield return new WaitUntil( () => res != null );
            Debug.Log("Receive JoinResponse");

            Assert.IsNotNull<JoinResponse>(res);
            Assert.IsTrue(res.player.id > 0);
            player2 = res.player;

            yield return new WaitUntil( () => initializeResponse != null );
            Debug.Log("Receive InitializeResponse");

            Assert.AreEqual(40, initializeResponse.state.ban.Length);
        }

        // InitializedRequest 送信
        {
            StartResponse res = null;
            eventHandler = CreateEventHandler<StartResponse>((StartResponse obj) => {
                res = obj;
                ws1.OnMessage -= eventHandler;
            });
            ws1.OnMessage += eventHandler;
            ws1.Send("initialized " + JsonUtility.ToJson(new EmptyRequest()));
            ws2.Send("initialized " + JsonUtility.ToJson(new EmptyRequest()));

            yield return new WaitUntil( () => res != null );
            Debug.Log("Receive StartResponse");

            Assert.IsTrue(res.current_time > 0);
            Assert.IsTrue(res.start_time > 0);

            Debug.Log("CurrentTime: " + res.current_time);
            Debug.Log("StartTime: " + res.start_time);
        }

        // MovablePositionsRequest 送信
        {
            MovablePositionResponse res = null;
            eventHandler = CreateEventHandler<MovablePositionResponse>((MovablePositionResponse obj) => {
                res = obj;
                ws1.OnMessage -= eventHandler;
            });
            ws1.OnMessage += eventHandler;

            ws1.Send("movable_positions " + JsonUtility.ToJson(new MovablePositionsRequest{
                position = new PositionInfo{ x = 4, y = 6 },
            }));

            yield return new WaitUntil( () => res != null );
            Debug.Log("Receive MovablePositionsResponse");

            Assert.AreEqual<int>(res.positions.Length, 1, "歩なので前のひとマスにだけ移動候補がある");
            Assert.AreEqual<int>(res.positions[0].position.y, 5, "前のマスにすすめる");
        }

        // MoveRequest の送信
        MoveResponse moveResponse = null;
        {
            // サーバー時間のセット送信
            ws1.Send("debug_set_time " + JsonUtility.ToJson(new DebugSetTimeRequest{
                time = initializeResponse.state.ban.Where((k) => k.position.x == 4 && k.position.y == 6).First().movable_time
            }));

            eventHandler = CreateEventHandler<MoveResponse>((MoveResponse obj) => {
                moveResponse = obj;
                ws1.OnMessage -= eventHandler;
            });
            ws1.OnMessage += eventHandler;

            ws1.Send("move " + JsonUtility.ToJson(new MoveRequest{
                from = new PositionInfo{ x = 4, y = 6 },
                to = new PositionInfo{ x = 4, y = 5 },
                nari = false,
            }));

            yield return new WaitUntil( () => moveResponse != null );
            Debug.Log("Receive MoveResponse");

            Assert.AreEqual<int>(moveResponse.from.x, 4);
            Assert.AreEqual<int>(moveResponse.from.y, 6);
            Assert.AreEqual<int>(moveResponse.to.x, 4);
            Assert.AreEqual<int>(moveResponse.to.y, 5);
            Assert.AreEqual<bool>(moveResponse.captured, false);
            Assert.AreEqual<int>(moveResponse.moved_koma.koma_base.koma_type, 8);
        }

        // MoveRequest で駒が取れた場合の検証
        {
            eventHandler = CreateEventHandler<MoveResponse>((MoveResponse obj) => moveResponse = obj );
            ws1.OnMessage += eventHandler;

            // 歩をすすめる
            ws1.Send("debug_set_time " + JsonUtility.ToJson(new DebugSetTimeRequest{ time = moveResponse.moved_koma.movable_time }));
            moveResponse = null;
            ws1.Send("move " + JsonUtility.ToJson(new MoveRequest{
                from = new PositionInfo{ x = 4, y = 5 },
                to = new PositionInfo{ x = 4, y = 4 },
                nari = false,
            }));
            yield return new WaitUntil( () => moveResponse != null );
            Debug.Log("Receive MoveResponse");

            // 歩をすすめる
            ws1.Send("debug_set_time " + JsonUtility.ToJson(new DebugSetTimeRequest{ time = moveResponse.moved_koma.movable_time }));
            moveResponse = null;
            ws1.Send("move " + JsonUtility.ToJson(new MoveRequest{
                from = new PositionInfo{ x = 4, y = 4 },
                to = new PositionInfo{ x = 4, y = 3 },
                nari = false,
            }));
            yield return new WaitUntil( () => moveResponse != null );
            Debug.Log("Receive MoveResponse");

            // 歩をすすめる
            ws1.Send("debug_set_time " + JsonUtility.ToJson(new DebugSetTimeRequest{ time = moveResponse.moved_koma.movable_time }));
            moveResponse = null;
            ws1.Send("move " + JsonUtility.ToJson(new MoveRequest{
                from = new PositionInfo{ x = 4, y = 3 },
                to = new PositionInfo{ x = 4, y = 2 },
                nari = true,
            }));
            yield return new WaitUntil( () => moveResponse != null );
            Debug.Log("Receive MoveResponse");

            Assert.AreEqual<bool>(true, moveResponse.captured, "相手の歩が取れた");
            Assert.AreEqual<int>(14, moveResponse.moved_koma.koma_base.koma_type, "と金に成った");

            ws1.OnMessage -= eventHandler;
        }
			
        // FinishResponse の検証
        {
            FinishResponse finishResponse = null;

            eventHandler = CreateEventHandler<MoveResponse>((MoveResponse obj) => {
                moveResponse = obj;
                ws1.OnMessage -= eventHandler;
                ws1.OnMessage += nextEventHandler;
            });
            nextEventHandler = CreateEventHandler<FinishResponse>((FinishResponse obj) => {
                finishResponse = obj;
                ws1.OnMessage -= nextEventHandler;
            });
            ws1.OnMessage += eventHandler;

            //ws1.Send("debug_set_time " + JsonUtility.ToJson(new DebugSetTimeRequest{ time = putKomaResponse.koma.movable_time }));
            moveResponse = null;
            ws1.Send("move " + JsonUtility.ToJson(new MoveRequest{
                from = new PositionInfo{ x = 4, y = 1 },
                to = new PositionInfo{ x = 4, y = 0 },
                nari = true,
            }));
            yield return new WaitUntil( () => moveResponse != null );
            Debug.Log("Receive MoveResponse");

            Assert.AreEqual<bool>(true, moveResponse.captured, "相手の王をとった");

            yield return new WaitUntil( () => finishResponse != null );
            Debug.Log("Receive FinishResponse");

            Assert.IsTrue(player1.id == finishResponse.winner.id, "プレイヤー1が勝利");
        }


        IntegrationTest.Pass();
    }
	
    void Initialize () {
        room_id = UnityEngine.Random.Range(0, 10000).ToString();
        ws1 = Connect();
        ws2 = Connect();
    }

    private WebSocket Connect() {
        var ws = new WebSocket(url);

        ws.OnOpen += (sender, e) => {
            Debug.Log("Websocket Open");
        };
        ws.OnError += (sender, e) => {
            Debug.Log("Websocket Error: " + e.Message);
        };
        ws.OnMessage += (sender, e) => {
            Debug.Log("Websocket Message: " + e.Data);
        };
        ws.OnClose += (sender, e) => {
            Debug.Log("Websocket Close ");
        };

        Debug.Log("Start Connect");
        ws.Connect();
        Debug.Log("End Connect");

        return ws;
    }

    IEnumerator WaitUntilListAdded (IList list, int count) {
        while (list.Count < count) {
            yield return new WaitForSeconds(0.1f);
        }
        yield return new WaitForSeconds(1f);
    }
}
